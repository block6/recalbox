## RECALBOX - SYSTEM AMIGA CD32 ##

Put your amiga CD32 roms in this directory.

Rom files must be files of the following types:
- ISO Images (*.iso c/ or w/o corresponding .cue)
- BIN Images (*.ipf c/ or w/o corresponding .cue)
Both may be zipped (iso/bin w or w/o its cue in the same zip)

UAE files (*.uae) may be used to specify configuration for a particular game.

This system requires BIOS to work (cd32.rom & cd32ext.rom)

Documentation can be found here : 
https://github.com/recalbox/recalbox-os/wiki/Amiga-emulation-on-Recalbox-(EN)
